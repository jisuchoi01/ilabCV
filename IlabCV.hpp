#include <cstdio>
#include <cstdlib>
#include <cstring>

namespace ILAB
{
    namespace cv
    {
        class Matrix
        {
        public:
            unsigned int row = 0;
            unsigned int col = 0;
            unsigned int channel = 0;
            int dataType = 0;
            unsigned char* data = NULL;

            /*
           *  constructor
           */
            Matrix ();

            Matrix (unsigned int row,
                    unsigned int col,
                    unsigned int channel,
                    int dataType,
                    unsigned char* data);

            /* public function */
            size_t GetDataSize(void);

            /*
            * destructor
            */
            ~Matrix ();
            
        private:
        };
    }
}
